package com.xd.dao;

import com.xd.model.CandidatePoint;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.LinkedList;

/**
 * designed by Matt
 *
 * @author Matt
 *         e-mail:ZHUHAN1401@126.com
 * @version JDK 1.8.0_101
 * @since 2017/6/25 14:17 星期日
 * Description: description
 */
@Repository
public interface MapMatchingDAO {

    LinkedList<CandidatePoint> importCandidatePoints();

    LinkedList<Double> findShortestPathLength(@Param("FacilityID")Long FacilityID, @Param("IncidentID") Long IncidentID);

    LinkedList<String> findShortestPathType(@Param("FacilityID")Long FacilityID, @Param("IncidentID") Long IncidentID);


}
