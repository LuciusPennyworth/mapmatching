package com.xd.model;

/**
 * designed by Matt
 *
 * @author Matt
 *         e-mail:ZHUHAN1401@126.com
 * @version JDK 1.8.0_101
 * @since 2017/6/10 11:30 星期六
 * Description: description
 */
public class Point {

    private Double longitude;
    private Double latitude;

    public Point(Double longitude, Double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        return "Point{" +
                "longitude=" + longitude +
                ", latitude=" + latitude +
                '}';
    }
}
