package com.xd.model;

/**
 * designed by Matt
 *
 * @author Matt
 *         e-mail:ZHUHAN1401@126.com
 * @version JDK 1.8.0_101
 * @since 2017/6/9 20:49 星期五
 * Description: description
 */
public class CandidateEdge {

    private CandidatePoint eStart;
    private CandidatePoint eEnd;
    private Double transPro;
    //时间分析属性值
    private Double temporalValue;
    //该条边对应的f值，同一终点会有多个f值 最后取最大
    private Double fValue;

    public CandidateEdge(CandidatePoint eStart, CandidatePoint eEnd) {
        this.eStart = eStart;
        this.eEnd = eEnd;
    }

    @Override
    public String toString() {
        return "CandidateEdge{" +
                "eStart=" + eStart +
                ", eEnd=" + eEnd +
                ", transPro=" + transPro +
                ", temporalValue=" + temporalValue +
                ", fValue=" + fValue +
                '}';
    }

    public Double getfValue() {
        return fValue;
    }

    public void setfValue(Double fValue) {
        this.fValue = fValue;
    }

    public CandidatePoint geteStart() {
        return eStart;
    }

    public void seteStart(CandidatePoint eStart) {
        this.eStart = eStart;
    }

    public CandidatePoint geteEnd() {
        return eEnd;
    }

    public void seteEnd(CandidatePoint eEnd) {
        this.eEnd = eEnd;
    }

    public Double getTransPro() {
        return transPro;
    }

    public void setTransPro(Double transPro) {
        this.transPro = transPro;
    }

    public Double getTemporalValue() {
        return temporalValue;
    }

    public void setTemporalValue(Double temporalValue) {
        this.temporalValue = temporalValue;
    }
}
