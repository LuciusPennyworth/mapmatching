package com.xd.model;


import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * designed by Matt
 *
 * @author Matt
 *         e-mail:ZHUHAN1401@126.com
 * @version JDK 1.8.0_101
 * @since 2017/6/9 20:48 星期五
 * Description: description
 */
public class GPSPoint{

    private Long objectID;
    private Integer nearX;
    private Integer nearY;
    private Date pTime;
    private LinkedList<CandidatePoint> candidatePoints;
    private HashMap<CandidatePoint,Double> pointsSTValues;

    public GPSPoint(Long objectID, Integer nearX, Integer nearY, Date pTime) {
        this.objectID = objectID;
        this.nearX = nearX;
        this.nearY = nearY;
        this.pTime = pTime;
        this.candidatePoints = new LinkedList<CandidatePoint>();
        this.pointsSTValues = new HashMap<CandidatePoint, Double>();
    }

    public void addCandidatePoint(CandidatePoint candidatePoint){
        candidatePoints.add(candidatePoint);
    }

    @Override
    public String toString() {
        return "GPSPoint{" +
                "objectID=" + objectID +
                ", nearX=" + nearX +
                ", nearY=" + nearY +
                ", pTime=" + pTime +
                ", candidatePoints=" + candidatePoints +
                ", pointsSTValues=" + pointsSTValues +
                '}';
    }

    public Long getObjectID() {
        return objectID;
    }

    public void setObjectID(Long objectID) {
        this.objectID = objectID;
    }

    public Integer getNearX() {
        return nearX;
    }

    public void setNearX(Integer nearX) {
        this.nearX = nearX;
    }

    public Integer getNearY() {
        return nearY;
    }

    public void setNearY(Integer nearY) {
        this.nearY = nearY;
    }

    public Date getpTime() {
        return pTime;
    }

    public void setpTime(Date pTime) {
        this.pTime = pTime;
    }

    public LinkedList<CandidatePoint> getCandidatePoints() {
        return candidatePoints;
    }

    public void setCandidatePoints(LinkedList<CandidatePoint> candidatePoints) {
        this.candidatePoints = candidatePoints;
    }

    public HashMap<CandidatePoint, Double> getPointsSTValues() {
        return pointsSTValues;
    }

    public void setPointsSTValues(HashMap<CandidatePoint, Double> pointsSTValues) {
        this.pointsSTValues = pointsSTValues;
    }
}
