package com.xd.model;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * designed by Matt
 *
 * @author Matt
 *         e-mail:ZHUHAN1401@126.com
 * @version JDK 1.8.0_101
 * @since 2017/6/9 20:49 星期五
 * Description: description
 */
public class CandidatePoint {

    private Long objectID;
    private Long infID;
    private Float nearDist;
    private Integer nearX;
    private Integer nearY;
    private LinkedList<Double> temFValue;
    private Double gaussDistribution;
    private Boolean flag;

    public CandidatePoint() {
        this.temFValue = new LinkedList<Double>();
        this.flag = false;
    }

    @Override
    public String toString() {
        return "CandidatePoint{" +
                "objectID=" + objectID +
                ", infID=" + infID +
                ", nearDist=" + nearDist +
                ", nearX=" + nearX +
                ", nearY=" + nearY +
                ", gaussDistribution=" + gaussDistribution +
                ", flag=" + flag +
                '}';
    }

    public LinkedList<Double> getTemFValue() {
        return temFValue;
    }

    public void setTemFValue(LinkedList<Double> temFValue) {
        this.temFValue = temFValue;
    }

    public Long getObjectID() {
        return objectID;
    }

    public void setObjectID(Long objectID) {
        this.objectID = objectID;
    }

    public Long getInfID() {
        return infID;
    }

    public void setInfID(Long infID) {
        this.infID = infID;
    }

    public Float getNearDist() {
        return nearDist;
    }

    public void setNearDist(Float nearDist) {
        this.nearDist = nearDist;
    }

    public Integer getNearX() {
        return nearX;
    }

    public void setNearX(Integer nearX) {
        this.nearX = nearX;
    }

    public Integer getNearY() {
        return nearY;
    }

    public void setNearY(Integer nearY) {
        this.nearY = nearY;
    }

    public Double getGaussDistribution() {
        return gaussDistribution;
    }

    public void setGaussDistribution(Double gaussDistribution) {
        this.gaussDistribution = gaussDistribution;
    }

    public Boolean getFlag() {
        return flag;
    }

    public void setFlag(Boolean flag) {
        this.flag = flag;
    }
}
