package com.xd.util;

import com.xd.dao.MapMatchingDAO;
import com.xd.model.CandidatePoint;
import com.xd.model.GPSPoint;
import com.xd.service.MapMatchingService;
import javafx.scene.input.DataFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * designed by Matt
 *
 * @author Matt
 *         e-mail:ZHUHAN1401@126.com
 * @version JDK 1.8.0_101
 * @since 2017/6/11 4:05 星期日
 * Description: description
 */
@Component
public class DataInputUtil {

    @Resource
    private MapMatchingService mapMatchingService;

    public LinkedList<CandidatePoint> getCandidatePoints(){
        return mapMatchingService.importCandidatePoints();
    }

    public Double findShortestPathLength(Long facId,Long IncId){
        return mapMatchingService.findShortestPathLength(facId,IncId).get(0);
    }

    public LinkedList<String> findShortestPathType(Long facId,Long IncId){
        return mapMatchingService.findShortestPathType(facId,IncId);
    }

}
