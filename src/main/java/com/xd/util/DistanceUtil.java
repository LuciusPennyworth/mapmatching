package com.xd.util;

import com.xd.model.Point;

import java.util.LinkedList;

/**
 * designed by Matt
 *
 * @author Matt
 *         e-mail:ZHUHAN1401@126.com
 * @version JDK 1.8.0_101
 * @since 2017/6/9 21:07 星期五
 * Description: 根据GPS坐标计算两点距离
 */
public class DistanceUtil {

    private static Double EARTH_RADIUS = 6378.137;

    private static Double rad(double d) {
        return d * Math.PI / 180.0;
    }

    /**
     * 根据经纬度计算两点距离
     * @param a
     * @param b
     * @return
     *//*
    public static Double calculateDistance(Point a,Point b){
        Double disLong=rad(a.getLongitude())-rad(b.getLongitude());

        Double radLat1=rad(a.getLatitude());
        Double radLat2=rad(b.getLatitude());
        Double disLati=radLat1-radLat2;

        Double distance=2 * Math.asin(Math.sqrt(Math.pow(Math.sin(disLati / 2), 2)
                + Math.cos(radLat1) * Math.cos(radLat2)
                * Math.pow(Math.sin(disLong / 2), 2)));
        distance*=EARTH_RADIUS;
        distance=Math.round(distance*10000d)/10000d;
        return distance*1000;
    }*/




}
