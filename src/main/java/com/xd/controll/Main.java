package com.xd.controll;

import com.xd.model.CandidatePoint;
import com.xd.model.GPSPoint;

import java.util.Date;
import java.util.LinkedList;

/**
 * designed by Matt
 *
 * @author Matt
 *         e-mail:ZHUHAN1401@126.com
 * @version JDK 1.8.0_101
 * @since 2017/6/26 5:08 星期一
 * Description: description
 */
public class Main {

    public static void main(String[] args) {
        STMatching stMatching=new STMatching();
        //初始化GPS点，设置时间，XY坐标
        GPSPoint point16 = new GPSPoint(16l, 453791, 4415759, new Date(1498406972l));
        GPSPoint point18 = new GPSPoint(18l, 453878, 4415455, new Date(1498407008l));
        GPSPoint point19 = new GPSPoint(19l, 454267, 4415482, new Date(1498407048l));
        GPSPoint point20 = new GPSPoint(20l, 454283, 4416422, new Date(1498407148l));

        LinkedList<GPSPoint> gpsPoints = new LinkedList<GPSPoint>();
        gpsPoints.add(point16);
        gpsPoints.add(point18);
        gpsPoints.add(point19);
        gpsPoints.add(point20);

        gpsPoints =stMatching.candidatePreparation(gpsPoints);
        gpsPoints =stMatching.calaTransProConstructEdge(gpsPoints);
        LinkedList<CandidatePoint> pointList =stMatching.resultMapMatching(gpsPoints);
        System.out.println(pointList.size());

    }

}
