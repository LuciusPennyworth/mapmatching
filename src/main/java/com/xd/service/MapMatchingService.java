package com.xd.service;

import com.xd.dao.MapMatchingDAO;
import com.xd.model.CandidatePoint;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.LinkedList;

/**
 * designed by Matt
 *
 * @author Matt
 *         e-mail:ZHUHAN1401@126.com
 * @version JDK 1.8.0_101
 * @since 2017/6/26 5:19 星期一
 * Description: description
 */
@Service
public class MapMatchingService {

    @Resource
    private MapMatchingDAO mapMatchingDAO;

    public LinkedList<CandidatePoint> importCandidatePoints(){
        return mapMatchingDAO.importCandidatePoints();
    }

    public LinkedList<Double> findShortestPathLength(Long FacilityID,Long IncidentID){
        return mapMatchingDAO.findShortestPathLength(FacilityID,IncidentID);
    }

    public LinkedList<String> findShortestPathType(Long FacilityID, Long IncidentID){
        return mapMatchingDAO.findShortestPathType(FacilityID,IncidentID);
    }

}
