package com.xd.util;

import com.xd.model.GPSPoint;
import com.xd.model.Point;
import org.junit.Test;

import java.util.Date;

/**
 * designed by Matt
 *
 * @author Matt
 *         e-mail:ZHUHAN1401@126.com
 * @version JDK 1.8.0_101
 * @since 2017/6/10 12:56 星期六
 * Description: description
 */
public class TestDistanceUtil {

    /*39.979598,116.338674,0,141,39746.2069444444,2008-10-25,04:58:00
    39.979699,116.33891,0,229,39746.2070023148,2008-10-25,04:58:05*/

    @Test
    public void testDistance(){
        Point a=new Point(116.33891,39.979699);
        Point b=new Point(116.338674,39.979598);
        //System.out.println(DistanceUtil.calculateDistance(a,b));;
    }

    @Test
    public void testTimeFormat(){
        GPSPoint point16 = new GPSPoint(16l, 453791, 4415759, new Date(1498406972l));//2017/6/26 0:9:32
        GPSPoint point18 = new GPSPoint(18l, 453878, 4415455, new Date(1498407008l));//2017/6/26 0:10:8
        System.out.println("time: "+ (point18.getpTime().getTime()-point16.getpTime().getTime()));
    }


}
